Simple Spring Boot example to resolve following scenario:

"Expose async APIs and permit the configuration of a max limit of parallel thread to process them, produce an error if limit is overcome"

## Run

```
./mvnw spring-boot:run
```

## License

This project is released under CC0 license (Creative Commons Zero)

