/*
    async-upper-bound
    Written in 2022 by Mirko Perillo
    To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
    You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
*/
package eu.owlcode.asyncupperbound.service;

import org.springframework.stereotype.Service;

@Service
public class Worker {

	public void longWork() {
		try {
			Thread.sleep(10_000);
			System.out.println("Work done by " + Thread.currentThread().getName());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
