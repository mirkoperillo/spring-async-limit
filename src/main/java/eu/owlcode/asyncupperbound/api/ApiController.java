/*
    async-upper-bound
    Written in 2022 by Mirko Perillo
    To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
    You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
*/
package eu.owlcode.asyncupperbound.api;

import java.io.IOException;
import java.util.concurrent.RejectedExecutionException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.owlcode.asyncupperbound.service.Worker;

@RestController
public class ApiController {

	@Autowired
	private Worker worker;

	@GetMapping("/api/worker")
	@Async
	public void elaborate() {
		worker.longWork();
	}

	@ExceptionHandler(RejectedExecutionException.class)
	public void handleRejectedExecution(HttpServletResponse res, Exception ex) throws IOException {
		res.sendError(HttpServletResponse.SC_BAD_REQUEST, "Too many elaborations");
	}

}
